# roam_like_at_home_hyperpaper

Hyperpaper version of the Mobicom2018 paper "Experience: Implications of Roaming in Europe"
https://dl.acm.org/citation.cfm?doid=3241539.3241577

If you use any of the resources here, please make an atribution by citing our work:

Anna Maria Mandalari, Andra Lutu, Ana Custura, Ali Safari Khatouni, Özgü Alay, Marcelo Bagnulo, Vaibhav Bajpai, Anna Brunstrom, Jörg Ott, Marco Mellia, and Gorry Fairhurst. 2018. 
*Experience: Implications of Roaming in Europe.* 
In Proceedings of the 24th Annual International Conference on Mobile Computing and Networking (MobiCom '18). 
ACM, New York, NY, USA, 179-189. DOI: https://doi.org/10.1145/3241539.3241577

For more resources related to the roaming in Europe project, please visit: https://www.it.uc3m.es/amandala/roaming.html

For mode details on what a hyperpaper is, please check the following: https://ccronline.sigcomm.org/wp-content/uploads/2019/02/sigcomm-ccr-final285.pdf

Alberto Dainotti, Ralph Holz, Mirja Kühlewind, Andra Lutu, Joel Sommers, and Brian Trammell. 
"Open collaborative hyperpapers: a call to action." 
ACM SIGCOMM Computer Communication Review 49, no. 1 (2019): 31-33.

